// @Injectable()
export class Common {
    constructor(http, formBuilder, router) {
        this.http = http;
        this.formBuilder = formBuilder;
        this.router = router;
        this.title_parent = "title_parent";
    }
    httpOptions() {
        return {
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json',
                'Access-Control-Allow-Methods': 'POST',
                'Access-Control-Allow-Headers': 'Content-Type, Authorization',
                'Authorization': $('meta[name=token]').attr('content') ? $('meta[name=token]').attr('content') : "token.123",
            }
        };
    }
}
//# sourceMappingURL=common.js.map