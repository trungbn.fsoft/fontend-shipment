import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Main } from './../core/main.component';
let HomeComponent = class HomeComponent extends Main {
    constructor() {
        super(...arguments);
        this.title = "Home";
    }
    ngOnInit() {
    }
};
HomeComponent = tslib_1.__decorate([
    Component({
        selector: 'app-home',
        templateUrl: './home.component.html',
        styleUrls: ['./home.component.css']
    })
], HomeComponent);
export { HomeComponent };
//# sourceMappingURL=home.component.js.map