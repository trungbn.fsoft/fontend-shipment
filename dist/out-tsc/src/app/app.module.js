import * as tslib_1 from "tslib";
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ShipmentComponent } from './shipment/shipment.module';
// import { Auth } from './auth/auth';
const appRoutes = [
    { path: 'login', component: LoginComponent },
    { path: '', component: HomeComponent },
    { path: 'home', component: HomeComponent },
    { path: 'contact', component: HomeComponent },
    { path: '**', component: PageNotFoundComponent }
];
let AppModule = class AppModule {
};
AppModule = tslib_1.__decorate([
    NgModule({
        declarations: [
            AppComponent,
            LoginComponent,
            HomeComponent,
            PageNotFoundComponent,
        ],
        imports: [
            ShipmentComponent,
            BrowserModule,
            FormsModule,
            HttpClientModule,
            ReactiveFormsModule,
            RouterModule.forRoot(appRoutes),
        ],
        providers: [],
        bootstrap: [AppComponent]
    })
], AppModule);
export { AppModule };
//# sourceMappingURL=app.module.js.map