import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { DetailComponent } from './detail/detail.component';
import { AddComponent } from './add/add.component';
import { ListComponent } from './list/list.component';
import { RouterModule } from "@angular/router";
const shipmentRoutes = [
    { path: 'shipment/detail/:ref', component: DetailComponent },
    { path: 'shipment/list', component: ListComponent },
    { path: 'shipment/list/page/:page', component: ListComponent },
    { path: 'shipment/add', component: AddComponent },
    { path: 'shipment/edit/:ref', component: AddComponent },
];
let ShipmentComponent = class ShipmentComponent {
    ngOnInit() {
    }
};
ShipmentComponent = tslib_1.__decorate([
    NgModule({
        imports: [
            CommonModule,
            FormsModule,
            ReactiveFormsModule,
            RouterModule.forChild(shipmentRoutes)
        ],
        declarations: [
            DetailComponent,
            AddComponent,
            ListComponent
        ],
        exports: [
            RouterModule
        ]
    })
], ShipmentComponent);
export { ShipmentComponent };
//# sourceMappingURL=shipment.module.js.map