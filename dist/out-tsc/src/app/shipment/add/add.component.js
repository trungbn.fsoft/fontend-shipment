import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Validators } from "@angular/forms";
import { Main } from './../../core/main.component';
let AddComponent = class AddComponent extends Main {
    constructor() {
        super(...arguments);
        this.mesenger = '';
        this.btn_add = false;
        this.base_url = 'http://localhost:8081';
        this.submitted = false;
        this.loading = false;
        this.loadingAdd = false;
        this.returnUrl = '/shipment/list';
        this.title = "Add shipment";
        this.shipment = {
            quote: {
                id: {
                    type: String,
                }
            },
            origin: {
                contact: {
                    name: "",
                    email: "",
                    phone: "",
                },
                address: {
                    country_code: "FR",
                    locality: "",
                    postal_code: "",
                    address_line1: "",
                }
            },
            destination: {
                contact: {
                    name: "",
                    email: "",
                    phone: "",
                },
                address: {
                    country_code: "FR",
                    locality: "",
                    postal_code: "",
                    address_line1: "",
                }
            },
            package: {
                dimensions: {
                    height: "",
                    width: "",
                    length: "",
                    unit: "cm",
                },
                grossWeight: {
                    amount: "",
                    unit: "kg",
                }
            },
            ref: "",
            cost: "",
        };
    }
    get f() {
        return this.formShipment.controls;
    }
    ngOnInit() {
        this.loadForm();
        this.ref = this.activatedRoute.snapshot.paramMap.get('ref');
        if (this.ref) {
            const url = this.base_url + "/api/shipment/getShipment/" + this.ref;
            this.http.get(url, this.httpOptions())
                .subscribe((res) => {
                this.shipment = res.data;
                this.loadForm();
            });
        }
    }
    loadForm() {
        this.formShipment = this.formBuilder.group({
            email: [this.shipment.origin.contact.email, [Validators.required, Validators.email]],
            name: [this.shipment.origin.contact.name, [Validators.required]],
            phone: [this.shipment.origin.contact.phone, [Validators.required]],
            locality: [this.shipment.origin.address.locality],
            postal_code: [this.shipment.origin.address.postal_code],
            country_code: [this.shipment.origin.address.country_code, [Validators.required]],
            address_line1: [this.shipment.origin.address.address_line1],
            receiver_name: [this.shipment.destination.contact.name, [Validators.required]],
            receiver_email: [this.shipment.destination.contact.email],
            receiver_phone: [this.shipment.destination.contact.phone, [Validators.required]],
            receiver_locality: [this.shipment.destination.address.locality, [Validators.required]],
            receiver_postal_code: [this.shipment.destination.address.postal_code],
            receiver_country_code: [this.shipment.destination.address.country_code, [Validators.required]],
            receiver_address_line1: [this.shipment.destination.address.address_line1, [Validators.required]],
            height: [this.shipment.package.dimensions.height],
            width: [this.shipment.package.dimensions.width],
            length: [this.shipment.package.dimensions.length],
            unit_cm: [this.shipment.package.dimensions.unit],
            unit: [this.shipment.package.grossWeight.unit],
            weight: [this.shipment.package.grossWeight.amount, [this.validWeight.bind(this), Validators.required, Validators.min(0.000001)]],
        });
    }
    validWeight(c) {
        if (c.value <= 15 && c.value >= 1) {
            return null;
        }
        else {
            return {
                validWeight: true
            };
        }
    }
    getQuote() {
        this.submitted = true;
        this.loading = true;
        const shipmentInput = this.formShipment.value;
        let body = this.getBody(this.shipment, shipmentInput);
        this.shipment = body.data;
        if (this.formShipment.invalid) {
            this.loading = false;
            return;
        }
        const url = this.base_url + "/api/shipment/getquote";
        this.http.post(url, body, this.httpOptions()).subscribe((res) => {
            let data;
            data = res.data[0];
            if (data.amount > 0) {
                this.shipment.cost = data.amount;
                this.shipment.quote.id = data.id;
                this.btn_add = true;
                this.mesenger = ` Giá ship cho đơn hàng của bạn ${res.data[0].amount} USD
                                     Vui lòng kích vào nút bên dưới để tạo đơn Ship
                                     `;
                this.btn_add = true;
            }
            else {
                this.btn_add = false;
                this.mesenger = `Không lấy được giá ship vui lòng kiểm tra giá trị nhập vào`;
            }
            this.loading = false;
        });
    }
    backGetQuote() {
        this.btn_add = false;
    }
    createShipment(ref = '') {
        this.submitted = true;
        this.loadingAdd = true;
        if (this.formShipment.invalid) {
            this.loadingAdd = false;
            return;
        }
        let body = {
            "data": this.shipment
        };
        const url = this.base_url + "/api/shipment/createshipment";
        this.http.post(url, body, this.httpOptions())
            .subscribe((res) => {
            let data;
            data = res.data;
            if (data.cost > 0) {
                this.mesenger = "Register shipment success";
            }
            else {
                this.mesenger = "Register shipment Error";
            }
            setTimeout(() => {
                this.loadingAdd = false;
                this.router.navigate([this.returnUrl]);
            }, 2000);
        });
    }
    getBody(shipment, shipmentInput) {
        shipment.origin.contact.name = shipmentInput.name;
        shipment.origin.contact.email = shipmentInput.email;
        shipment.origin.contact.phone = shipmentInput.phone;
        shipment.origin.address.country_code = shipmentInput.country_code;
        shipment.origin.address.locality = shipmentInput.locality;
        shipment.origin.address.postal_code = shipmentInput.postal_code;
        shipment.origin.address.address_line1 = shipmentInput.address_line1;
        shipment.destination.contact.name = shipmentInput.receiver_name;
        shipment.destination.contact.email = shipmentInput.receiver_email;
        shipment.destination.contact.phone = shipmentInput.receiver_phone;
        shipment.destination.address.country_code = shipmentInput.receiver_country_code;
        shipment.destination.address.locality = shipmentInput.receiver_locality;
        shipment.destination.address.postal_code = shipmentInput.receiver_postal_code;
        shipment.destination.address.address_line1 = shipmentInput.receiver_address_line1;
        shipment.package.dimensions.height = shipmentInput.height;
        shipment.package.dimensions.width = shipmentInput.width;
        shipment.package.dimensions.length = shipmentInput.length;
        shipment.package.dimensions.unit = shipmentInput.unit_cm;
        shipment.package.grossWeight.amount = shipmentInput.weight;
        shipment.package.grossWeight.unit = shipmentInput.unit;
        return {
            "data": shipment
        };
    }
};
AddComponent = tslib_1.__decorate([
    Component({
        selector: 'app-add',
        templateUrl: './add.component.html',
        styleUrls: ['./add.component.css']
    })
], AddComponent);
export { AddComponent };
//# sourceMappingURL=add.component.js.map