import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
// import {ActivatedRoute} from "@angular/router";
import { Main } from './../../core/main.component';
let ListComponent = class ListComponent extends Main {
    constructor() {
        super(...arguments);
        this.api_base_url = 'http://localhost:8081';
        this.listShipment = [];
        this.pagination = [];
        this.title = "List shipment";
    }
    ngOnInit() {
        this.loadPagination(1);
        setTimeout(() => {
            const url_total = this.api_base_url + "/api/shipment/getShipmentListTotal";
            this.http.get(url_total, this.httpOptions()).subscribe((res) => {
                this.numPage = Math.ceil(res.data.count / 10);
                for (let i = 0; i < this.numPage; i++) {
                    this.pagination.push(i + 1);
                }
            });
        }, 100);
    }
    filterShipment() {
        console.log("filterShipment");
    }
    loadPagination(page) {
        this.pageCurrent = page;
        const url = this.api_base_url + "/api/shipment/getShipmentList/?page=" + page;
        this.http.get(url, this.httpOptions()).subscribe((res) => {
            this.listShipment = res.data;
        });
    }
    deleteShipment() {
        const url = this.api_base_url + "/api/shipment/delete/" + this.ref;
        this.http.delete(url, this.httpOptions()).subscribe((res) => {
            $('#id_' + this.ref).empty();
            this.loadPagination(this.pageCurrent);
            this.ref = '';
            // window.location.reload();
        });
    }
    cancelDeleteShipment() {
        this.ref = '';
    }
    comfirmDelete(ref) {
        this.ref = ref;
        $('#myModal_delete').modal('show');
    }
};
ListComponent = tslib_1.__decorate([
    Component({
        selector: 'app-list',
        templateUrl: './list.component.html',
        styleUrls: ['./list.component.css']
    })
], ListComponent);
export { ListComponent };
//# sourceMappingURL=list.component.js.map