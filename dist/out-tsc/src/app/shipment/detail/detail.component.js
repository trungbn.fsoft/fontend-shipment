import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Main } from './../../core/main.component';
let DetailComponent = class DetailComponent extends Main {
    constructor() {
        super(...arguments);
        this.title = "Detail shipment";
        this.shipment = {
            quote: {
                id: { type: String }
            },
            origin: {
                contact: {
                    name: { type: String },
                    email: { type: String },
                    phone: { type: String },
                },
                address: {
                    country_code: { type: String },
                    locality: { type: String },
                    postal_code: { type: Number, default: 0 },
                    address_line1: { type: String },
                }
            },
            destination: {
                contact: {
                    name: { type: String },
                    email: { type: String },
                    phone: { type: String },
                },
                address: {
                    country_code: { type: String },
                    locality: { type: String },
                    postal_code: { type: Number, default: 0 },
                    address_line1: { type: String }
                }
            },
            package: {
                dimensions: {
                    height: { type: Number, default: 0 },
                    width: { type: Number, default: 0 },
                    length: { type: Number, default: 0 },
                    unit: { type: String }
                },
                grossWeight: {
                    amount: { type: Number, default: 0 },
                    unit: { type: String },
                }
            },
            ref: { type: String },
            cost: { type: String },
        };
        this.base_url = 'http://localhost:8081';
    }
    ngOnInit() {
        this.activatedRoute.params.subscribe(params => {
            // console.log(params);
            // this.ref = this.activatedRoute.snapshot.paramMap.get('ref');
            this.ref = params.ref;
            const url = this.base_url + "/api/shipment/getShipment/" + this.ref;
            this.http.get(url, this.httpOptions())
                .subscribe((res) => {
                // console.log(res);
                this.shipment = res.data;
            });
        });
    }
};
DetailComponent = tslib_1.__decorate([
    Component({
        selector: 'app-detail',
        templateUrl: './detail.component.html',
        styleUrls: ['./detail.component.css']
    })
], DetailComponent);
export { DetailComponent };
//# sourceMappingURL=detail.component.js.map