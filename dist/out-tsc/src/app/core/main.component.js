import { Common } from './../core/common';
export class Main {
    constructor(httpClient, 
    // private FormGroup: FormGroup, 
    // private Validators: Validators, 
    FormBuilder, Router, ActivatedRoute) {
        this.httpClient = httpClient;
        this.FormBuilder = FormBuilder;
        this.Router = Router;
        this.ActivatedRoute = ActivatedRoute;
        // export 
        this.http = this.httpClient;
        this.formBuilder = this.FormBuilder;
        this.router = this.Router;
        this.activatedRoute = this.ActivatedRoute;
        this.common = new Common;
    }
    // method
    httpOptions() {
        return {
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json',
                'Access-Control-Allow-Methods': 'POST',
                'Access-Control-Allow-Headers': 'Content-Type, Authorization',
                'Authorization': $('meta[name=token]').attr('content') ? $('meta[name=token]').attr('content') : "token.123",
            }
        };
    }
}
//# sourceMappingURL=main.component.js.map