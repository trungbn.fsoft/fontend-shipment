import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { Validators } from '@angular/forms';
import { Main } from './../core/main.component';
let LoginComponent = class LoginComponent extends Main {
    constructor() {
        super(...arguments);
        this.mesenger = '';
        this.base_url = 'http://localhost:8081';
        this.title = 'Login';
        this.submitted = false;
        this.loading = false;
        this.returnUrl = '/home';
    }
    // convenience getter for easy access to form fields
    get f() {
        return this.formLogin.controls;
    }
    ngOnInit() {
        this.formLogin = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', [
                    Validators.required,
                    //   Validators.pattern(this.passwordPattern),
                    Validators.minLength(6)
                ]]
        });
    }
    onSubmit() {
        console.log(this.httpOptions());
        this.loading = true;
        this.submitted = true;
        const { email, password } = this.formLogin.value;
        if (this.formLogin.invalid) {
            this.loading = false;
            return;
        }
        const url = `${this.base_url}/user/login`;
        if (email != '' && password != '') {
            let body = {
                "email": email,
                "password": password
            };
            this.http.post(url, body, this.httpOptions()).subscribe((res) => {
                console.log(body);
                let user = res.data;
                if (user['id']) {
                    console.log(user);
                    this.auth.setlogin(user);
                    this.mesenger = 'Login Success';
                    setTimeout(() => {
                        this.loading = false;
                        let navigationExtras = {
                            queryParamsHandling: 'preserve',
                            preserveFragment: true
                        };
                        this.router.navigateByUrl(this.returnUrl, navigationExtras);
                    }, 2000);
                }
                else {
                    this.loading = false;
                    this.mesenger = 'Email & password not exits';
                }
            });
        }
        else {
            this.loading = false;
            this.mesenger = 'Email & password not empty';
        }
    }
    // hiddenItem(id) {
    //     if ($('#' + id).attr('data-status') == 1) {
    //         $('#' + id).hide();
    //         $('#' + id).attr('data-status', 0);
    //     } else {
    //         $('#' + id).show();
    //         $('#' + id).attr('data-status', 1);
    //     }
    // }
    setValue() {
    }
};
tslib_1.__decorate([
    Input()
], LoginComponent.prototype, "childMessage", void 0);
LoginComponent = tslib_1.__decorate([
    Component({
        selector: 'app-login',
        templateUrl: './login.component.html',
        styleUrls: ['./login.component.css']
    })
], LoginComponent);
export { LoginComponent };
//# sourceMappingURL=login.component.js.map