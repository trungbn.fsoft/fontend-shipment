import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Validators } from '@angular/forms';
import { Main } from './core/main.component';
let AppComponent = class AppComponent extends Main {
    constructor() {
        super(...arguments);
        this.title = 'F - Shipment';
        this.title_parent = 'F - Shipment title_parent';
        this.isLogged = false;
        this.base_url = 'http://localhost:8081';
    }
    onActivate(componentReference) {
        this.title = componentReference.title;
        this.checkLogin();
        $('meta[name=token]').attr('content', localStorage.getItem('token'));
    }
    ngOnInit() {
        this.checkLogin();
        this.formSearch = this.formBuilder.group({
            ref: ['', [Validators.required]],
        });
    }
    checkLogin() {
        this.isLogged = localStorage.getItem('user_id') == null ? false : true;
        this.user_name = localStorage.getItem('user_name');
        if (this.isLogged == false) {
            this.router.navigate(['../login']);
        }
    }
    logout() {
        this.auth.logout();
        this.isLogged = false;
        let navigationExtras = {
            queryParamsHandling: 'preserve',
            preserveFragment: true
        };
        this.router.navigateByUrl('/', navigationExtras);
    }
    searchRef() {
        this.router.navigate([`/shipment/detail/${this.formSearch.value.ref}`]);
    }
};
AppComponent = tslib_1.__decorate([
    Component({
        selector: 'app-root',
        templateUrl: './app.component.html',
        styleUrls: ['./app.component.css']
    })
], AppComponent);
export { AppComponent };
//# sourceMappingURL=app.component.js.map