import { Component, NgModule, OnInit, Input } from '@angular/core';
import { Auth } from './../auth/auth';
import { Main } from './../core/main.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent extends Main {
  auth = new Auth;
  mesenger = '';
  title = 'Login';
  formLogin;
  submitted = false;
  loading = false;

  get f() {
    return this.formLogin.controls;
  }

  ngOnInit() {
    if (this.isLogged === true) {
      this.router.navigate(['../home']);
    }
    this.formLogin = this.formGroup;
    this.formLogin = this.formBuilder.group({
      email: ['', [this.validators.required, this.validators.email]],
      password: ['', [
        this.validators.required,
        this.validators.minLength(this.constant.lengthPassword)
      ]]
    });
  }

  onSubmit() {
    this.loading = true;
    this.submitted = true;
    const { email, password } = this.formLogin.value;
    if (this.formLogin.invalid) {
      this.loading = false;
      return;
    }
    const url = this.baseUrlApi('user/login');
    if (email !== '' && password !== '') {
      const body = {
         email,
         password
      };
      this.http.post(url, body, this.httpOptions()).subscribe((res: any) => {
        const user = res.data;
        if (user.id) {
          console.log(user);
          this.auth.setlogin(user);
          this.mesenger = 'Login Success';
          setTimeout(() => {
            this.loading = false;
            this.router.navigateByUrl('/home', this.navigationExtras);
          }, 2000);
        } else {
          this.loading = false;
          this.mesenger = 'Email & password not exits';
        }
      });
    } else {
      this.loading = false;
      this.mesenger = 'Email & password not empty';
    }
  }
}
