import {Component, OnInit} from '@angular/core';
import { Main } from './../../core/main.component';

@Component({
    selector: 'app-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.css']
})

export class DetailComponent extends Main {
    title = "Detail shipment";
    ref: string;
    shipment = {
        quote: {
            id: {type: String}
        },
        origin: {
            contact: {
                name: {type: String},
                email: {type: String},
                phone: {type: String},
            },
            address: {
                country_code: {type: String},
                locality: {type: String},
                postal_code: {type: Number, default: 0},
                address_line1: {type: String},
            }
        },
        destination: {
            contact: {
                name: {type: String},
                email: {type: String},
                phone: {type: String},
            },
            address: {
                country_code: {type: String},
                locality: {type: String},
                postal_code: {type: Number, default: 0},
                address_line1: {type: String}
            }
        },
        package: {
            dimensions: {
                height: {type: Number, default: 0},
                width: {type: Number, default: 0},
                length: {type: Number, default: 0},
                unit: {type: String}
            },
            grossWeight: {
                amount: {type: Number, default: 0},
                unit: {type: String},
            }
        },
        ref: {type: String},
        cost: {type: String},
    };

    
    ngOnInit() {
        this.activatedRoute.params.subscribe(
            params => {
                this.ref = params.ref;
                const url = this.baseUrlApi('api/shipment/getShipment/'+this.ref);
                this.http.get(url,this.httpOptions())
                    .subscribe((res: any) => {
                        // console.log(res);
                        this.shipment = res.data;
                    });
            }
        );
    }
}
