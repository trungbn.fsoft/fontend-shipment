import { Component, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { DetailComponent } from './detail/detail.component';
import { AddComponent } from './add/add.component';
import { ListComponent } from './list/list.component';

import { RouterModule, Routes } from '@angular/router';

// @Component({
//     selector: 'app-shipment',
//     templateUrl: './shipment.component.html'
// })

const shipmentRoutes: Routes = [
  { path: 'shipment/detail/:ref', component: DetailComponent },
  { path: 'shipment/list', component: ListComponent },
  { path: 'shipment/list/page/:page', component: ListComponent },
  { path: 'shipment/add', component: AddComponent },
  { path: 'shipment/edit/:ref', component: AddComponent },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(shipmentRoutes)
  ],
  declarations: [
    DetailComponent,
    AddComponent,
    ListComponent
  ],
  exports: [
    RouterModule
  ]
})
export class ShipmentComponent {
  ngOnInit() {

  }
}
