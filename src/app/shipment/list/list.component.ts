import { Component, OnInit } from '@angular/core';
// import {ActivatedRoute} from "@angular/router";
import { Main } from './../../core/main.component';

declare var $: any;

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})

export class ListComponent extends Main {

  listShipment = [];
  pagination = [];
  pageCurrent: number;
  ref: string;
  page: number;
  numPage: number;
  title = 'List shipment';

  ngOnInit() {
    console.log(this.title);
    this.loadPagination(1);
    setTimeout(() => {
      const url_total = this.baseUrlApi('api/shipment/getShipmentListTotal');
      this.http.get(url_total, this.httpOptions()).subscribe((res: any) => {
        this.numPage = Math.ceil(res.data.count / 10);
        for (let i = 0; i < this.numPage; i++) {
          this.pagination.push(i + 1);
        }
      });
    }, 100);


  }

  filterShipment() {
    console.log('filterShipment');
  }

  loadPagination(page) {
    this.pageCurrent = page;
    const url = this.baseUrlApi("api/shipment/getShipmentList/?page=" + page);
    this.http.get(url, this.httpOptions()).subscribe((res: any) => {
      this.listShipment = res.data;
    });
  }

  deleteShipment() {
    const url = this.baseUrlApi("api/shipment/delete/" + this.ref);
    this.http.delete(url, this.httpOptions()).subscribe((res: any) => {
      $('#id_' + this.ref).empty();
      this.loadPagination(this.pageCurrent);
      this.ref = '';
      // window.location.reload();
    });
  }

  cancelDeleteShipment() {
    this.ref = '';
  }

  comfirmDelete(ref) {
    this.ref = ref;
    $('#myModal_delete').modal('show');
  }
}
