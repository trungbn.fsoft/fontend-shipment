declare var $: any;
export class Auth {

    setlogin(user) {
        localStorage.setItem('user_id', user.id);
        localStorage.setItem('user_email', user.email);
        localStorage.setItem('user_name', user.name);
        localStorage.setItem('user_role', user.role);
        localStorage.setItem('token', user.token);
        localStorage.setItem('avatar', user.avatar);
        $('meta[name=token]').attr('content', user.token);
    }

    logout() {
        localStorage.removeItem('user_id');
        localStorage.removeItem('token');
        localStorage.removeItem('user_email');
        localStorage.removeItem('user_name');
        localStorage.removeItem('user_role');
        localStorage.removeItem('avatar');
    }
}
