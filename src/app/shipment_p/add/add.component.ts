import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Main } from '../../core/main.component';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent extends Main {
  mesenger = '';
  btn_add = false;
  formPersonal;
  submitted = false;
  loading = false;
  loadingAdd = false;
  ref: string;
  title = 'Add Personal';
  cd: ChangeDetectorRef;
  fileUpload: File = null;
  personal = {
    name: '',
    image: '',
    image2: '',
    address: '',
    created_at: '',
    ref: '',
  };

  objImage = {
    image : '',
    image2 : '',
  };

  get f() {
    return this.formPersonal.controls;
  }

  // tslint:disable-next-line: use-lifecycle-interface
  ngOnInit() {
    this.formPersonal = this.formGroup;
    this.loadForm();
    this.ref = this.getPram('ref');
    if (this.ref) {
      const url = this.constant.baseUrlApi + '/api/personal/getPersonal/' + this.ref;
      this.http.get(url, this.httpOptions())
        .subscribe((res: any) => {
          this.personal = res.data;
          this.loadForm();
        });
    }
  }


  loadForm() {
    this.formPersonal = this.formBuilder.group({
      name: [this.personal.name, [this.validators.required]],
      address: [this.personal.address],
      image: [null],
      image2: [null],
    });
  }

  onFileChange(event, type) {
    this.fileUpload = event.target.files;
    console.log(type);

    const reader = new FileReader();
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.objImage[type] =  reader.result;
        console.log(this.objImage);
        this.formPersonal.patchValue(this.objImage);
      };
      // need to run CD since file load runs outside of zone
      // this.cd.markForCheck();

    }
  }


  createPersonal(ref = '') {
    this.submitted = true;
    this.loadingAdd = true;
    if (this.formPersonal.invalid) {
      this.loadingAdd = false;
      return;
    }


    const personalInput = this.formPersonal.value;


    const body = this.getBody(this.personal, personalInput);
    console.log(body);
    const url = this.baseUrlApi('api/personal/createPersonal');
    this.http.post(url, body, this.httpOptions())
      .subscribe((res: any) => {
        let data: any;
        data = res.data;
        if (data.ref) {
          this.mesenger = 'Register Personal success';
        } else {
          this.mesenger = 'Register Personal Error';
        }
        setTimeout(() => {
          this.loadingAdd = false;
          this.router.navigate(['/personal/list']);
        }, 2000);
      });
  }

  getBody(personal, personalInput) {
    personal.name = personalInput.name;
    personal.address = personalInput.address;
    personal.image = personalInput.image;
    personal.image2 = personalInput.image2;
    return {
      data: personal
    };
  }


}
