import { Component, OnInit } from '@angular/core';
// import {ActivatedRoute} from "@angular/router";
import { Main } from '../../core/main.component';

declare var $: any;

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})

export class ListComponent extends Main {

  listPersonal = [];
  pagination = [];
  pageCurrent: number;
  ref: string;
  page: number;
  numPage: number;
  title = "List personal";


  ngOnInit() {
    console.log('fdsf')
    this.loadPagination(1);
    setTimeout(() => {
      const url_total = this.baseUrlApi('api/personal/getPersonalListTotal');
      this.http.get(url_total, this.httpOptions()).subscribe((res: any) => {
        this.numPage = Math.ceil(res.data.count / 10);
        for (let i = 0; i < this.numPage; i++) {
          this.pagination.push(i + 1);
        }
      });
    }, 100);


  }

  loadPagination(page) {
    this.pageCurrent = page;
    const url = this.baseUrlApi('api/personal/getPersonalList/?page=' + page);
    this.http.get(url, this.httpOptions()).subscribe((res: any) => {
      this.listPersonal = res.data;
    });
  }

  deletePersonal() {
    const url = this.baseUrlApi('api/personal/delete/' + this.ref);
    this.http.delete(url, this.httpOptions()).subscribe((res: any) => {
      $('#id_' + this.ref).empty();
      this.loadPagination(this.pageCurrent);
      this.ref = '';
      // window.location.reload();
    });
  }

  cancelDeletePersonal() {
    this.ref = '';
  }

  comfirmDelete(ref) {
    this.ref = ref;
    $('#myModal_delete').modal('show');
  }

  addInfo(ref) {
    $('#myModal_info').modal('show');
  }

}
