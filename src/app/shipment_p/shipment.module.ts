import { Component, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { DetailComponent } from './detail/detail.component';
import { AddComponent } from './add/add.component';
import { ListComponent } from './list/list.component';

import { RouterModule, Routes } from '@angular/router';

const shipmentRoutes: Routes = [
  { path: 'personal/detail/:ref', component: DetailComponent },
  { path: 'personal/list', component: ListComponent },
  { path: 'personal/list/page/:page', component: ListComponent },
  { path: 'personal/add', component: AddComponent },
  { path: 'personal/edit/:ref', component: AddComponent },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(shipmentRoutes)
  ],
  declarations: [
    DetailComponent,
    AddComponent,
    ListComponent
  ],
  exports: [
    RouterModule
  ]
})
export class PersonalComponent {
  // tslint:disable-next-line: use-lifecycle-interface
  ngOnInit() {
  }
}
