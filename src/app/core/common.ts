import { DatePipe } from '@angular/common';

export class Common {

  title = 'Common';

  description = 'Common';


  /**
   * Check Format Date
   * https://angular.io/api/common/DatePipe
   * @input date : 2019-08-13T07:23:07.659Z
   * @input format : dd-MM-yyyy hh:mm:ss a
   * @output dateFormat : 13-08-2019 02:23:07 PM
   */
  //
  formatDate(date = '', format = 'dd-MM-yyyy hh:mm:ss a') {
    if (date === '') {
      const today = new Date();
      date = today.toISOString();
    }

    const datePipe = new DatePipe('en-US');
    const valueDatePipe = datePipe.transform(date, format);
    return valueDatePipe;

  }

  /**
   * Check Format Date
   * https://angular.io/api/common/DatePipe
   * @input date : 2019-08-13T07:23:07.659Z
   * @input format : dd-MM-yyyy hh:mm:ss a
   * @output dateFormat : 13-08-2019 02:23:07 PM
   */
  //
  formatDateInt(intDate = 0, format = 'dd-MM-yyyy hh:mm:ss a') {
    if (intDate === 0) {
      intDate = new Date().getTime();
    }
    const date = new Date(intDate).toString();
    const datePipe = new DatePipe('en-US');
    const valueDatePipe = datePipe.transform(date, format);
    return valueDatePipe;
  }
}
