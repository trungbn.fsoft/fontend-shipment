import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { Common } from './../core/common';
import { Constant } from './../core/constant';
import { Auth } from './../auth/auth';
import { Socket } from 'ngx-socket-io';


declare var $: any;

export class Main {
    constructor(
        private httpClient: HttpClient,
        // private FormGroup: FormGroup,
        // private Validators: Validators,
        // tslint:disable-next-line: no-shadowed-variable
        private FormBuilder: FormBuilder,
        // tslint:disable-next-line: no-shadowed-variable
        private Router: Router,
        // tslint:disable-next-line: no-shadowed-variable
        private ActivatedRoute: ActivatedRoute,
        // tslint:disable-next-line: no-shadowed-variable
        private Socket: Socket
    ) {  }

    // export
    http = this.httpClient;

    socket = this.Socket;

    formBuilder = this.FormBuilder;

    router = this.Router;

    activatedRoute = this.ActivatedRoute;

    // tslint:disable-next-line: new-parens
    common = new Common;

    // tslint:disable-next-line: new-parens
    constant = new Constant;

    formGroup = FormGroup;

    validators = Validators;

    // tslint:disable-next-line: new-parens
    auth = new Auth;

    isLogged = localStorage.getItem('user_id') == null ? true : true;

    navigationExtras: NavigationExtras = {
        queryParamsHandling: 'preserve',
        preserveFragment: true
    };

    user_name = localStorage.getItem('user_name');

    user_avatar = localStorage.getItem('avatar');

    token = localStorage.getItem('token');


    // get getPram from url
    getPram(param) {
        return this.activatedRoute.snapshot.paramMap.get(param);
    }

    // get queryParamMap from url
    get(queryParam) {
        return this.activatedRoute.snapshot.queryParamMap.get(queryParam);
    }


    // method
    httpOptions() {
        return {
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json',
                'Access-Control-Allow-Methods': 'POST',
                'Access-Control-Allow-Headers': 'Content-Type, Authorization',
                Authorization: $('meta[name=token]').attr('content') ? $('meta[name=token]').attr('content') : "token.123",
            }
        };
    }
    baseUrl(uri = '') {
        return this.constant.baseUrl + '/' + uri;
    }
    baseUrlApi(uri = '') {
        return this.constant.baseUrlApi + '/' + uri;
    }

    logout() {
        console.log('logout');
        this.auth.logout();
        this.isLogged = false;
        this.router.navigateByUrl('/login', this.navigationExtras);
    }

    checkLogin() {
        this.isLogged = localStorage.getItem('user_id') == null ? false : true;
        this.user_name = localStorage.getItem('user_name');
        if (this.isLogged === false) {
            this.router.navigate(['../login']);
        }
    }
}
