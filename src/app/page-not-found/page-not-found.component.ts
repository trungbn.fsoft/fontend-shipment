import { Component, OnInit } from '@angular/core';
import { Main } from './../core/main.component';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.css']
})
export class PageNotFoundComponent extends Main {

  ngOnInit() {
    console.log('page-not-found')
  }

}
