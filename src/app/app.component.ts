import { Component } from '@angular/core';
import { Main } from './core/main.component';

declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent extends Main {
  title = 'Shipment';
  titleParent = 'F - Shipment';

  onActivate(componentReference) {
    this.title = componentReference.title;
    // this.checkLogin();
    // this.setHead();
  }

  setHead() {
    $('meta[name=token]').attr('content', localStorage.getItem('token'));
    $('body').find('h1.title').html(this.title);
  }

  // tslint:disable-next-line: use-lifecycle-interface
  ngOnInit() {
    // this.checkLogin();
    // this.setHead();
  }
}
