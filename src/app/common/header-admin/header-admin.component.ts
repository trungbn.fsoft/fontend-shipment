import { Component, OnInit } from '@angular/core';
import {Main} from './../../core/main.component';

@Component({
    selector: 'app-header-admin',
    templateUrl: './header-admin.component.html',
    styleUrls: ['./header-admin.component.css']
})

export class HeaderAdminComponent extends Main {

    formSearch;
    title = "Home"
    ngOnInit() {
        // this.checkLogin();

        this.user_name = localStorage.getItem('user_name');

        this.formSearch = this.formGroup;

        this.formSearch = this.formBuilder.group({
                ref: ['', [this.validators.required, this.validators.email]],
        });
    }

    // onActivate() {
    //     this.checkLogin();
    //     console.log("active")
    // }

    searchRef() {
        console.log("searchRef")
        this.router.navigate([`/shipment/detail/${this.formSearch.value.ref}`]);
    }


}
