import { Component, OnInit } from '@angular/core';
import {Main} from './../../core/main.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent extends Main {
    formSearch; 
    title = "Login"

    ngOnInit() {

        this.checkLogin();

        // this.user_name = localStorage.getItem('user_name');

        this.formSearch = this.formGroup;

        this.formSearch = this.formBuilder.group({
                ref: ['', [this.validators.required, this.validators.email]],
        }); 

    }
    
    // onActivate() {

    //     this.checkLogin();

    //     console.log("active")

    // }
    
    
    
    searchRef() {
        console.log("searchRef") 
        this.router.navigate([`/shipment/detail/${this.formSearch.value.ref}`]);
    }
    
     
}
