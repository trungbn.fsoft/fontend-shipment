import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ShipmentComponent } from './shipment/shipment.module';
import { PersonalComponent } from './shipment_p/shipment.module';
import { HeaderComponent } from './common/header/header.component';
import { HeaderAdminComponent } from './common/header-admin/header-admin.component';
import { FooterComponent } from './common/footer/footer.component';
import { ChatComponent } from './chat/inbox/inbox.component';
import { ListChatComponent } from './chat/list/list.component';

// SocketIoModule
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
const config: SocketIoConfig = { url: 'http://localhost:8081', options: {} };
const appRoutes: Routes = [
  { path: 'shipment', component: ShipmentComponent },
  { path: 'personal', component: PersonalComponent },
  { path: 'chat/inbox/:id', component: ChatComponent },
  { path: 'chat/list', component: ListChatComponent },
  { path: 'login', component: LoginComponent },
  { path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent },
  { path: 'contact', component: HomeComponent },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    ChatComponent,
    PageNotFoundComponent,
    HeaderComponent,
    HeaderAdminComponent,
    FooterComponent,
    ListChatComponent,
  ],
  imports: [
    ShipmentComponent,
    PersonalComponent,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes),
    SocketIoModule.forRoot(config)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
