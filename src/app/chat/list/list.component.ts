import {Component, OnInit} from '@angular/core';
// import {ActivatedRoute} from "@angular/router";
 import { Main } from './../../core/main.component';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListChatComponent  extends Main {

    listUser = [];
    pagination = [];
    pageCurrent : number;
    ref : string;
    page : number;
    numPage: number;

  ngOnInit() {
    this.loadPagination(1);
    setTimeout(() => {
        const url_total = this.baseUrlApi("user/getlist");
        this.http.get(url_total,this.httpOptions()).subscribe((res: any) => {
            this.numPage = Math.ceil(res.data.count / 10);
            for (let i = 0; i < this.numPage; i++) {
                this.pagination.push(i + 1);
            }
        });
    }, 100);
  }

  loadPagination(page) {
    this.pageCurrent = page;
    const url = this.baseUrlApi("user/getlist");
    this.http.get(url,this.httpOptions()).subscribe((res: any) => {
        this.listUser = res.data;
    });
}

}
