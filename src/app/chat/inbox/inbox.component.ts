import { Component, OnInit } from '@angular/core';
import { Main } from '../../core/main.component';
declare let $: any;

@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.css']
})
export class ChatComponent extends Main {

  title = 'Chat';
  listMessenge: any;
  userReceive;
  userReceiveId;
  userId;
  body = {
    user_send: null,
    user_receive: null,
    msg: null,
  };

  // tslint:disable-next-line: use-lifecycle-interface
  ngOnInit() {
    this.userReceiveId = this.getPram('id');
    this.userId = localStorage.getItem('user_id');
    this.body = {
      user_send: this.userId,
      user_receive: this.userReceiveId,
      msg: '',
    };
    this.getList();

    // get userReceive
    const urlGetUser = this.baseUrlApi('user/getUser');
    this.http.post(urlGetUser, { id: this.userReceiveId }, this.httpOptions()).subscribe((res: any) => {
      const user = res.data[0];
      if (user.id) {
        this.userReceive = user;
      }
    });

    this.socket.on('messages',  (data) => {
      this.sendNewMessenger(data);
    });
  }

   sendNewMessenger(msg) {
    const htmlMsg = `<div class='direct-chat-msg ${msg.user_send === this.userId ? 'right' : ''}'>
            <div class='direct-chat-info clearfix'>
            <span class='direct-chat-name pull-left'>${msg.user_send === this.userId ? this.user_name : this.userReceive.name}</span>
            <span class='direct-chat-timestamp pull-right'>${this.common.formatDate(msg.time)}</span>
            </div>
            <!-- /.direct-chat-info -->
            <img class='direct-chat-img'
            src='${msg.user_send === this.userId ? this.user_avatar : this.userReceive.avatar}'
            alt='message user image'>
            <!-- /.direct-chat-img -->
            <div class='direct-chat-text'>
            ${msg.msg}
            </div>
            <!-- /.direct-chat-text -->
        </div>`;
    $('#boxMessages').append(htmlMsg);
  }

  getList() {
    const url = this.baseUrlApi('api/chat/list-messenge');
    this.http.post(url, this.body, this.httpOptions()).subscribe((res: any) => {
      this.listMessenge = res.data;
      this.scrollTop();
    });
  }

  sendMessenger() {
    const msg = $('#text_messenger').val();
    $('#text_messenger').val('');
    const url = this.baseUrlApi('api/chat/add-messenge');
    this.body.msg = msg;
    this.socket.emit('messages', this.body);
    this.http.post(url, this.body, this.httpOptions()).subscribe((res: any) => {
      this.scrollTop();
    });
  }

  scrollTop() {
    const boxMessages = document.getElementById('boxMessages');
    boxMessages.scrollTop = boxMessages.scrollHeight;
  }
}
